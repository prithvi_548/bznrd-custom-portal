import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { IonRouterOutlet, IonApp, IonSplitPane, IonPage, IonMenu, IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonItem, IonMenuButton } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import SavedUnits from './screens/saved_units/SavedUnits';
import SavedSearches from './screens/saved_serches/SavedSearches';
import ARPortal from './screens/ar_portal/ARPortal';
//import InvoiceDetail from './screens/ar_portal/components/InvoiceDetail';
import TestRedux from './screens/TestRedux';
//import SideMenu from './components/SideMenu';
import MenuContent from './components/MenuContent';
import Home from './screens/nav/Home';
 
const Routes: React.FC = () => {
  return (
      
    /*     <Router>
        <div id="app">
          <IonApp>
            <IonSplitPane contentId="main">
              <SideMenu/>
              <IonPage id="main">
                <Switch>
              
                <Route path="/" component={SavedUnits} exact={true} />
                <Route path="/saved-searches" component={SavedSearches}  />
                <Route   path="/ar-portal" component={ARPortal}  />
                <Route   path="/test-redux" component={TestRedux}  />
                </Switch>
              </IonPage>
            </IonSplitPane>
          </IonApp>
        </div>
      </Router> */
  
      <> 
       <IonMenu side="start" contentId="main-content" type="overlay">
          <IonHeader>
             <IonToolbar color="secondary">
                <IonMenuButton slot="end" color="primary" />
              </IonToolbar>
            </IonHeader>
            <MenuContent />
         </IonMenu>

        <IonPage id="main-content">
           <IonReactRouter>
              <IonRouterOutlet>
               
                 <Route path="/" component={ARPortal}   />
                 
                 <Route path="/saved-searches" component={SavedSearches}   />
                 <Route path="/saved-units" component={SavedUnits}   />
                 <Route path="/home" component={Home}   />
                 <Route path="/test-redux" component={TestRedux}  />
                 
              </IonRouterOutlet>
           </IonReactRouter>
        </IonPage>
    </>
 
  
      )
   }
  

export default Routes;