import React, { useState } from 'react';
import {  IonContent, IonList, IonItem } from '@ionic/react';

const MenuContent = () => {

    return (
        
      <IonContent>
        <IonList>
          <IonItem  routerLink="/" >Dashboard</IonItem>
          <IonItem  routerLink="/" >AR Portal</IonItem>
          <IonItem routerLink="/saved-searches" >Saved Searches</IonItem>
          <IonItem routerLink="/saved-units" >Saved Units</IonItem>
          <IonItem routerLink="/home" >Home</IonItem>
        </IonList>
      </IonContent>
     
    )
}
export default  MenuContent;

  