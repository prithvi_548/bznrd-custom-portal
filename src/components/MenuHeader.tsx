
import React, { useState } from 'react';
import {  IonContent, IonList, IonItem, IonHeader, IonToolbar, IonMenuButton } from '@ionic/react';

const MenuHeader = () => {

    return (
        

<IonHeader>
   <IonToolbar color="primary">
      {/* <StyledLogo /> */}
    
      <IonMenuButton slot="start" color="secondary" />
   </IonToolbar>
</IonHeader>
    )
}
export default  MenuHeader;
