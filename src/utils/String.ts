// import DateTimeFormatOptions from "Intl";

export const String = ( () => {


    const toLocaleStringSupportsLocales = () => {
        // from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
        try {
            return new Date().toLocaleString('i');
        } catch (error) {
            return error instanceof RangeError;
        }
        return false;
    }


    return {

        convertToFloat: (str: string, convertToDollar = false, noOfDecimalDigits = 2) => {

            var no = parseFloat(str).toFixed(noOfDecimalDigits);

            if (convertToDollar)
                no = '$' + no;

            return no;

        },
        convertToInt: (str: string) => {

            var no = parseInt(str);
            return no;

        },
        convertToDate: (mySqlDateTimeStr: string|Date, showTime: boolean = false, options: Intl.DateTimeFormatOptions = {}) => {

            const date = mySqlDateTimeStr instanceof Date ? mySqlDateTimeStr : new Date(mySqlDateTimeStr);
            const locales = 'en-US';

            const dateString = showTime
                ? toLocaleStringSupportsLocales()
                    ? date.toLocaleString(locales, options)
                    : date.toLocaleString()
                : toLocaleStringSupportsLocales()
                    ? date.toLocaleDateString(locales, options)
                    : date.toLocaleDateString();

            return dateString;
        }

    }
})();