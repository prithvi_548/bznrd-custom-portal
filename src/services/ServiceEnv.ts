export const ServiceEnv = ( () => {

    return {
        initalize: () => {
            require('dotenv').config();
        },
        apiBaseUrl: process.env.REACT_APP_API_BASE_URL,

    }
})();