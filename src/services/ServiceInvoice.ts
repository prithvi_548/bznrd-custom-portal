import {IInvoiceApiListInvoiceServerResponse, IInvoiceState} from "../types/Invoices";

export const ServiceInvoice = (() => {

    const TYPE_Payment = 'Payment';
    const TYPE_Invoice = 'Invoice';
    const TYPE_Core_Credit = 'Core Credit';
    const TYPE_Cash_Payment = 'Cash Payment';
    const TYPE_Credit_Note = 'Credit Note';
    const TYPE_Customer_Credit = 'Customer Credit';
    const TYPE_Interest_Charge = 'Interest Charge';


    return {


        isTypePayment: (invoice: IInvoiceApiListInvoiceServerResponse) => {

            const invType = invoice.type;
            const arr = [TYPE_Cash_Payment, TYPE_Payment];
            if (arr.includes(invType))
                return true;
            return false;
        },

        isTypeCharge: (invoice: IInvoiceApiListInvoiceServerResponse) => {

            const invType = invoice.type;
            if (invType == TYPE_Interest_Charge)
                return true;
            return false;
        },
        isTypeInvoice: (invoice: IInvoiceApiListInvoiceServerResponse) => {

            const invType = invoice.type;
            if (invType == TYPE_Invoice)
                return true;

            return false;

        },

        // isTypeCredit: (invoice: IInvoiceApiListInvoiceServerResponse) => {

        //     const invType = invoice.type.toLowerCase();
        //     if (invType.indexOf('credit') !== -1)
        //         return true;

        //     return false;

        // },


    }
})();