import {IInvoiceApiListInvoiceServerResponse, IInvoiceState} from "../types/Invoices";
import {ServiceInvoice} from "./ServiceInvoice";

export const ServiceInvoices = (() => {


    return {


        calculateAmount: (invoices: IInvoiceState[]) => {


            let amount = 0.0;
            for (let key of invoices) {
              // console.log('got called.');
                const partialAmt = key.partialAmt;
                const invoice = key.invoice;
               // console.log( partialAmt);
                
                const balance =   parseFloat(invoice.balance); //parseFloat(String(invoice.balance));
                
                if(partialAmt > 0){

                    if(ServiceInvoice.isTypeInvoice(invoice)){
                        amount += parseFloat(String(partialAmt));
                    }
                    // else if(ServiceInvoice.isTypeCredit(invoice)){

                    //     amount -= parseFloat(String(partialAmt));

                    // }
                }
                else{
                
                     
                      if(ServiceInvoice.isTypeInvoice(invoice)){
                        amount += balance;
                    }
                  //  else if(ServiceInvoice.isTypeCredit(invoice)){
                       // amount -= balance;

                  //  }
                }



            }

          //    console.log("SeviceInvoices totalAmt = " + amount + " invoices = ");
          //  console.log(invoices);

            amount = parseFloat(amount.toFixed(2));

            return amount;
        }

    }
})();