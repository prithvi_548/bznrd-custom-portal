import {ServiceEnv} from "./ServiceEnv";
import faker from 'faker';

import {
    IInvoiceApiListInvoiceServerResponse,
    IInvoiceApiListRequest,
    
} from "../types/Invoices";


export const ServiceInvoiceApi = ( () => {

    const apiBasUrl = ServiceEnv.apiBaseUrl;
    const g_invoicesUrl = apiBasUrl + '/invoices/index/';
    return {
        // getInvoices: (params?: IInvoiceApiListRequest):Promise<IInvoiceApiListServerResponse>  => {
        getInvoices: (params?: IInvoiceApiListRequest):Promise<IInvoiceApiListInvoiceServerResponse[]>  => {

            let myParams = params||{};

            // console.log('myParams ')

            // myParams = JSON.stringify(myParams);

            if(!myParams.start)
                myParams.start = 0;
            if(!myParams.limit)
                myParams.limit = 10;

            // REplace Faker
            // return axios.get(g_invoicesUrl, {
            //     params: {
            //         ...myParams
            //     }
            // })
            // .then( (response ) => {

            //     const data = response.data;

            //     let myResponse: IInvoiceApiListServerResponse = {
            //         invoices: [],
            //         totalInvoices: 0
            //     };// = [];

            //     for(let inv of data.invoices){

            //         inv.id = parseInt(inv.id);
            //         inv.totalAmt = parseFloat(inv.totalAmt);
            //         inv.balance = parseFloat(inv.balance);
            //         inv.isLocked = inv.isLocked ? true: false;

            //         myResponse.invoices.push(inv);
            //     }
            //     myResponse.totalInvoices = parseInt(data.totalInvoices);

            //     // console.log('response = ');
            //     // console.log(response);
            //     return myResponse;


            // })
                        return new Promise((resolve, reject) => {
                             // wrap in timeout to simulate server api call
                               setTimeout( () => {
                                    let data: IInvoiceApiListInvoiceServerResponse[] = [];
                                    for(let i = 0; i < 10; i++){

                                        let invoice: IInvoiceApiListInvoiceServerResponse  = {
                                            id: faker.random.number(),
                                            invNo: faker.random.uuid(),
                                            invDt: faker.date.past(10),
                                            customerPo: faker.name.firstName(),
                                            reference: faker.random.uuid(),
                                            arType:  'Invoice',
                                            totalAmt:  faker.finance.amount(),
                                            balance:  faker.finance.amount(),
                                            type: 'Invoice',
                                            arTrans: faker.random.uuid(),
                                            pdfLink: faker.internet.url(),
                                            // isLocked: boolean
                                            // lockedAmt: number,
                                            status: 'Processing' ,
                                            paragraph :faker.lorem.paragraph(),
                                            //Other attributes come here...
                                        };

                                        //  let invoice = new Invoice();
                                        // invoice.id = faker.random.uuid();
                                        //  invoice.date = faker.date.past(10);
                                        // invoice.reference = faker.random.uuid();
                                        // invoice.customerPo = faker.name.firstName();
                                        // invoice.arType = 'Invoice';
                                        //  invoice.originalAmt = faker.finance.amount();
                                        //  invoice.balance = faker.finance.amount();
                 
                 
                                         data.push(invoice);
                 
                                    }
                 
                                     // const perPageData = DATA.slice(start, perPage + start);
                 
                                    return resolve(data);
                 
                                }, 1000);
                 
                 
                 
                             });






        },
    }
})();