 
import {reducerTest} from './test';
import {reducerInvoices} from './invoices';
// import {InitialState as ThemeInitialState} from './theme/InitialState';
// import {reducerTheme} from "./theme/reducer";
import {InitialState as InvoicesInitialState} from './invoices/InitialState';
import {InitialState as TestInitialState} from './test/InitialState';
import {applyMiddleware, combineReducers, createStore} from "redux";

// export
const intialState = {
    //'theme': ThemeInitialState,
    invoices: InvoicesInitialState,
    test : TestInitialState
};

// console.log('initialState = ');
// console.log(intialState);

const reducerRoot = combineReducers({
    invoices: reducerInvoices,
    test: reducerTest
});


let middleware: any[] = [];//[thunk];

// if (__DEV__) {
//     // const reduxImmutableStateInvariant = require('redux-immutable-state-invariant')();
//     const logger = createLogger({collapsed: true});
//     middleware = [...middleware, logger];
// } else {
//     middleware = [...middleware];
// }


export const store = createStore(
    reducerRoot,
    intialState,
    applyMiddleware(...middleware)
);
