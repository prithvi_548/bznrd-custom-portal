import { InitialState } from "./InitialState";
import { INVOICES_ADD, INVOICES_REMOVE, INVOICES_REMOVE_ALL, INVOICES_TEST } from "./Action";
import { ServiceInvoices } from "../../services/ServiceInvoices";
import { IInvoiceApiListInvoiceServerResponse ,  IInvoiceState } from "../../types/Invoices";
//import {ServiceInvoiceApi} from "../../services/ServiceInvoiceApi";

export function reducerInvoices(state = InitialState, action: {
    type: any;
    data: {
        invoice: IInvoiceApiListInvoiceServerResponse ;
        partialAmt: number | null | undefined;
        partialPaymentExplanation: string | null;
        disputeExplanation: string | null;
        invoiceId: number;
        amount: any;
    };
}) {

    switch (action.type) {

        case INVOICES_ADD: {

            let newState = Object.assign({}, state);


            let doInvoiceExist = false;
            let existingInvEntry: IInvoiceState;
            for (let inv of newState.invoices) {

                if (inv.invoice.id == action.data.invoice.id) {
                    doInvoiceExist = true;
                    existingInvEntry = inv;
                    existingInvEntry.partialAmt = action.data.partialAmt;
                    existingInvEntry.partialPaymentExplanation = action.data.partialPaymentExplanation;
                    existingInvEntry.disputeExplanation = action.data.disputeExplanation
                    break;
                }

            }
            
            if (!doInvoiceExist) {
                newState.invoices.push({
                    invoice: action.data.invoice,
                    partialAmt: action.data.partialAmt,
                    partialPaymentExplanation: action.data.partialPaymentExplanation,
                    disputeExplanation: action.data.disputeExplanation
                });
            }


            const amount = ServiceInvoices.calculateAmount(newState.invoices);


            newState.amount = amount;
            console.log(newState);
            return newState;

        }
        case INVOICES_REMOVE: {

            let newState = Object.assign({}, state);

            let invoices: IInvoiceState[] = [];
            for (let inv of newState.invoices) {

                if (inv.invoice.id != action.data.invoiceId) {

                    invoices.push(inv);
                }
            }

            newState.invoices = invoices;

            const amount = ServiceInvoices.calculateAmount(newState.invoices);


            newState.amount = amount;
            console.log(newState);
            return newState;

        }
        case INVOICES_REMOVE_ALL: {

            let newState = Object.assign({}, state);
            newState.invoices = [];

            const amount = ServiceInvoices.calculateAmount(newState.invoices);


            newState.amount = amount;

            return newState;

        }


        case INVOICES_TEST:

            return {
                ...state,
                amount: action.data.amount
            };

        default:
            return state;
    }
}