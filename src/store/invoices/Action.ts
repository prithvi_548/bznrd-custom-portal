import {IInvoiceApiListInvoiceServerResponse , IInvoiceApiListRequest} from "../../types/Invoices";
//import {ServiceInvoiceApi} from "../../services/ServiceInvoiceApi";

export const INVOICES_ADD = 'INVOICES_ADD';
export const INVOICES_REMOVE = 'INVOICES_REMOVE';
export const INVOICES_REMOVE_ALL = 'INVOICES_REMOVE_ALL';
export const INVOICES_TEST = 'INVOICES_TEST';
 export const INVOICES_FILTER_FROM_DATE = 'INVOICES_FILTER_FROM_DATE';


export const ActionInvoices = (() => {

    return {

        add: (invoice: IInvoiceApiListInvoiceServerResponse, partialAmt: number = 0, partialPaymentExplanation: string = "" , disputeExplanation: string = "") => {

            return {
                type: INVOICES_ADD,
                data: {
                    invoice,
                    partialAmt,
                    partialPaymentExplanation,
                    disputeExplanation
                }
            };
        },
        remove: (invoiceId: number) => {
            return {
                type: INVOICES_REMOVE,
                data: {
                    invoiceId
                }
            };
        },
        removeAll: () => {

            return {
                type: INVOICES_REMOVE_ALL,
                data: {


                }
            };
        },
        filter_from_date: ( fromDate: string = "") => {
                return {
                type: INVOICES_FILTER_FROM_DATE,
                data: {
                    data: {
                        fromDate
                     }
                }

            }
        },
         

    }

})();
