
import {InitialState} from "./InitialState";
import {TEST_CHANGE_AMOUNT, TEST_CHANGE_LABEL} from "./Action";

export function reducerTest(state = InitialState, action: { type: any; data: { amount: number; }; }) {

    switch (action.type) {

        case TEST_CHANGE_AMOUNT:



            let nowAmount = action.data.amount - 10;


            console.log("state = ");
            console.log(state);

            return {

                ...state,
                ...action.data,
                ...{amount: nowAmount}


            };



        case TEST_CHANGE_LABEL:


            return {

                ...state,
                ...action.data


            };

        default:
            return state;
    }
}