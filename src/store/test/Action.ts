export const TEST_CHANGE_LABEL = 'TEST_CHANGE_LABEL';
export const TEST_CHANGE_AMOUNT = 'TEST_CHANGE_AMOUNT';
export const ActionTest = (() => {

    return {

        changeLabel: (label: string) => {

            return {
                type: TEST_CHANGE_LABEL,
                data: {
                    label: label
                }
            };
        },
        changeAmount: (amount: number) => {

            return {
                type: TEST_CHANGE_AMOUNT,
                data: {
                    amount: amount
                }
            };
        }
    }

})();
