
export interface IPaymentsListServerReponse_Payment {
    id: number,
    userID: number,
    amt: number,
    creditAmt: number,
    paymentType: string,
    paymentStatus: string,
    transactionID: string,
    usercustomerVaultID: string,
    createdOn: string,
    kenworthPaymentStatus: string,
    ts: string,

}
export interface IPaymentsListServerReponse {

    payments: IPaymentsListServerReponse_Payment[],
    count: number
    filters: {
       start: number,
       limit: number

   },
}
