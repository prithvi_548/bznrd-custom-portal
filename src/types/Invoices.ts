export const PAYMENT_TYPE_card = 'card';
export const PAYMENT_TYPE_cheque = 'cheque';

export const PAYMENT_STATUS_pending = 'pending';
export const PAYMENT_STATUS_processing = 'processing';
export const PAYMENT_STATUS_completed = 'completed';
export const PAYMENT_STATUS_failed = 'failed';
export const PAYMENT_STATUS_fraud = 'fraud';
export const PAYMENT_STATUS_on_hold = 'on-hold';
export const PAYMENT_STATUS_under_review = 'under-review';

export const KENWORTH_PAYMENT_STATUS_processing = 'processing';
export const KENWORTH_PAYMENT_STATUS_completed = 'completed';

 


export interface IInvoiceApiListRequest {
    start?: number,
    limit?: number,
    fromDate?: Date,
    toDate?: Date,
    filterBalance?: 'pending'|'paid',
    filterDays?: 'Current' | '31-60' | '61-90' | 'Over90'
}


export interface IInvoiceApiListInvoiceServerResponse {
  //  partialPayment: boolean;

    id: number,
    invNo: string,
    invDt: Date,
    customerPo: string,
    reference: string,
    arType: string,
    totalAmt: string
    balance: string
    type: string
    arTrans: string,
    pdfLink: string,
    // isLocked: boolean
    // lockedAmt: number,
    status: 'Processing' | 'Paid' | 'Available' | 'Outstanding',
    paragraph : string

}


export interface IInvoiceApiListServerResponse {

    invoices: IInvoiceApiListInvoiceServerResponse[],
    totalInvoices: number
}

export interface IInvoiceState {
    invoice: IInvoiceApiListInvoiceServerResponse,
    partialAmt?: number|null,
    partialPaymentExplanation: string|null
    disputeExplanation: string|null
}
export interface IInvoicesState {
    invoices: IInvoiceState[],
    amount: number|null,
    filters:IInvoiceApiListRequest
}

export interface IInvoiceListGridData {
    [key: number]: IInvoiceApiListInvoiceServerResponse
}

export interface IInvoiceListDrawerCurrentDrawer {
    type: 'payUsePartial'|'dispute',
    record: IInvoiceState|IInvoiceApiListInvoiceServerResponse
}

export interface IUserInfoCard {
    className?: string,
    cardTitle?: string,

    customerID: number,
    email: string,
    businessName: string,
    nickName: string,
    address: string,
    city: string,
    state: string,
    phone: string
}