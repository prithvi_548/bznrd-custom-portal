import {IInvoiceApiListServerResponse, IInvoicesState} from "./Invoices";
import {ITest} from "./Test";

export interface AppState{ // extends ReduxState{
    // theme: ITheme;
     invoices: IInvoicesState,
    test: ITest

}
