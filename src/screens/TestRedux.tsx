import React, { Component } from "react";
import { connect, ConnectedProps, useSelector, useDispatch } from 'react-redux';
import { AppState } from "../types/AppState";
import { ActionTest } from "../store/test";

type TProps = {
    amount: number
};


const TestRedux = (props: TProps) => {
    const state = useSelector((state: AppState) => state.test);
    const dispatch = useDispatch();
    const [amount, setAmount] = React.useState(state.amount);


    const handleAmountChanged = (event: { target: { value: any; }; }) => {

        setAmount(event.target.value);
    };

    const handleClicked = () => {
        dispatch(ActionTest.changeAmount(amount))
        setAmount(state.amount);
    };

    return (
        <div>

            <p style={{ color: "red" }}>Amount after Deduction is : {state.amount}</p>


            <input type="text" value={ state.amount} onChange={handleAmountChanged} />

            <button onClick={handleClicked}>Click Me</button>



        </div>
    );


}

export default TestRedux;