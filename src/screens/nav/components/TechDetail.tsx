import React from 'react';

import { IonHeader, IonButton, IonToolbar, IonButtons, IonBackButton, IonTitle, IonContent, IonIcon } from '@ionic/react';
import IonReactNav from './IonReactNav';
import MenuHeader from '../../../components/MenuHeader';
//import SecondDetail from './SecondDetail'

interface TechDetailProps {
  title: string;
  description: string;
  color: string;
  icon: string;
}

// const handleButtonClick = (e: React.MouseEvent) => {
//   const targetEl = e.target as HTMLIonButtonElement
//   const navEl = targetEl.closest('ion-nav')

//   if (navEl) {
//     navEl.push('nav-detail')
//   }
// }

const TechDetail: React.FC<TechDetailProps> = ({ title, description, color, icon }) => {

  return (
    <>
    <MenuHeader />
    <React.Fragment>
      <IonHeader translucent>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/home"></IonBackButton>
          </IonButtons>
          <IonTitle>{title}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-padding">
        <IonIcon size="large" icon={icon} style={{ color: `${color}` }} />
        <p>{description}</p>
        {/* <IonButton onClick={(e) => handleButtonClick(e)}>
          Go deeper
        </IonButton> */}
      </IonContent>
    </React.Fragment>
    </>
  );
};

export default TechDetail