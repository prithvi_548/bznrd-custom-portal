import { 
  IonContent,
  IonPage, 
  IonTitle, 
  IonToolbar, 
  IonList,
  IonItem,
  IonCheckbox,
  IonLabel,
  IonNote,
  IonBadge,
  IonFab,
  IonFabButton,
  IonIcon,
  IonCardContent,
  IonCard,
  IonCardTitle
} from '@ionic/react';
import React from 'react';
import { add } from 'ionicons/icons';
import { RouteComponentProps } from 'react-router';
import MenuHeader from '../../components/MenuHeader';

const SavedUnits: React.FC<RouteComponentProps> = (props) => {
  return (
    <>
      <MenuHeader />
      <IonContent  >
       <IonCard>
         Saved Units
       </IonCard>
      </IonContent>
    </>
  );
};

export default SavedUnits;