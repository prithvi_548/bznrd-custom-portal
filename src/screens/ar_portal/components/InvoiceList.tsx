import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import {
    IonCard, IonCardContent, IonGrid, IonRow, IonCol, IonContent, useIonViewWillEnter,
    IonInfiniteScroll, IonInfiniteScrollContent, IonCheckbox, IonLabel, IonButton, IonList, IonItem
} from '@ionic/react';
import { IInvoiceApiListInvoiceServerResponse } from '../../../types/Invoices';
import { ServiceInvoiceApi } from '../../../services/ServiceInvoiceApi';
import Filters from './Filters';
import { ActionInvoices } from '../../../store/invoices';
import { AppState } from '../../../types/AppState';
import IonReactNav from './IonReactNav';
import InvoiceDetail from './InvoiceDetail';



const InvoiceList: React.FC = () => {
    const state = useSelector((state: AppState) => state.invoices);
    const dispatch = useDispatch();
    const [items, setItems] = useState<IInvoiceApiListInvoiceServerResponse[]>([]);

    const [disableInfiniteScroll, setDisableInfiniteScroll] = useState<boolean>(false);
    //const invoices = ServiceInvoiceApi.getInvoices();

     const [invoice, setInvoices] = useState ();

    async function fetchInvoices(reset?: boolean) {
        const invoices: IInvoiceApiListInvoiceServerResponse[] = await ServiceInvoiceApi.getInvoices();

        if (invoices && invoices.length > 0) {
            setItems([...items, ...invoices]);
            setDisableInfiniteScroll(invoices.length < 10);
        } else {
            setDisableInfiniteScroll(true);
        }

    }
    // useEffect( () => {
    //     fetchInvoices();
    // } , []);


    useIonViewWillEnter(async () => {
        await fetchInvoices();
    });

    async function searchNext($event: CustomEvent<void>) {
        await fetchInvoices();

        ($event.target as HTMLIonInfiniteScrollElement).complete();
    }
 


   

    return (
        <>
        
       {/*      {state.amount > 0 && (<IonCard>
                <IonCardContent>
                    <div>
                        <IonButton color="primary">Pay   {`${state.amount}`}</IonButton>

                    </div>
                </IonCardContent>
            </IonCard>)} */}
            <IonCard>
                <IonCardContent>
                <IonReactNav detail={() => <InvoiceDetail  {...invoice} />} >
                        <IonList>

                            {items.map((invoice: IInvoiceApiListInvoiceServerResponse, i: number) => {
                                return <IonItem button className="ion-react-nav-detail-btn" key={i} onClick={() => setInvoices(invoice[i])}>
                                    <IonLabel>{invoice.id}</IonLabel>
                                    <IonLabel>{invoice.invNo}</IonLabel>
                                    <IonLabel>{invoice.arType}</IonLabel>
                                </IonItem>
                            })}

                            <IonInfiniteScroll threshold="100px" disabled={disableInfiniteScroll}
                                onIonInfinite={(e: CustomEvent<void>) => searchNext(e)}>
                                <IonInfiniteScrollContent
                                    loadingText="Loading more invoices...">
                                </IonInfiniteScrollContent>
                            </IonInfiniteScroll>
                        </IonList>
                     
                     </IonReactNav>
                </IonCardContent>
            </IonCard>
        </>
    )
}

export default InvoiceList;