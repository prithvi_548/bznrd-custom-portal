import React from 'react';

import { IonHeader, IonButton, IonToolbar, IonButtons, IonBackButton, IonTitle, IonContent, IonIcon } from '@ionic/react';
import IonReactNav from './IonReactNav';
import MenuHeader from '../../../components/MenuHeader';
//import SecondDetail from './SecondDetail'
import { IInvoiceApiListInvoiceServerResponse } from '../../../types/Invoices';
 

// const handleButtonClick = (e: React.MouseEvent) => {
//   const targetEl = e.target as HTMLIonButtonElement
//   const navEl = targetEl.closest('ion-nav')

//   if (navEl) {
//     navEl.push('nav-detail')
//   }
// }

const InvoiceDetail: React.FC<IInvoiceApiListInvoiceServerResponse> = (Invoices) => {

  return (
    <>
    <MenuHeader />
    <React.Fragment>
      <IonHeader translucent>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/"></IonBackButton>
          </IonButtons>
          <IonTitle>Title</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-padding">
        
        <p>Paragraph</p>
        {/* <IonButton onClick={(e) => handleButtonClick(e)}>
          Go deeper
        </IonButton> */}
      </IonContent>
    </React.Fragment>
    </>
  );
};

export default InvoiceDetail