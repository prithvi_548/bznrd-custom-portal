import React, { useState } from 'react';
import { IonCard, IonCardTitle, IonCardContent, IonGrid, IonRow, IonCol } from '@ionic/react';


const UserInfo: React.FC = () => {
  return (
    <>
      <IonCard>
        <IonCardTitle>My Account</IonCardTitle>
        <IonCardContent>
          <p>WERDCO BC INC</p>
          <p>4660 FLIPPIN ST,</p>
          <p>LAS VEGAS, NV 89115</p>
        </IonCardContent>
      </IonCard>


      <IonCard>
        <IonCardContent>
          <IonGrid>
            <IonRow>
              <IonCol size-md="3">
                <div>
                  Customer
                                </div>
              </IonCol>
              <IonCol size-md="3">
                <div>
                  Date
                                </div>
              </IonCol>
              <IonCol size-md="3"  >
                <div>
                  Total Outstanding
                              </div>
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol size-md="3">
                <div>
                  120
                                </div>
              </IonCol>
              <IonCol size-md="3">
                <div>
                  23/11/2020
                                </div>
              </IonCol>
              <IonCol size-md="3" >
                <div>
                  30.00
                              </div>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonCardContent>
      </IonCard>


    </>
  )
}
export default UserInfo;