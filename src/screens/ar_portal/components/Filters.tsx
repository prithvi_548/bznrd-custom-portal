import React, { useState } from 'react';


import { IonCard, IonCardTitle, IonCardContent, IonGrid, IonRow, IonCol, IonLabel, IonDatetime, IonSelectOption, IonSelect } from '@ionic/react';


const Filters: React.FC = () => {
  const [filterBy, setFilterBy] = useState<string>();
  const [selectedDate, setSelectedDate] = useState<string>('2012-12-15');
  return (
    <>
     <IonCard>
        <IonCardContent>
      <IonGrid>
        <IonRow>
          <IonCol size-md="3">
            <IonLabel>current</IonLabel>
          </IonCol>
          <IonCol size-md="3">
            <IonLabel>31-60 Days</IonLabel>
          </IonCol>
          <IonCol size-md="3"  >
            <IonLabel>61-90 Days</IonLabel>
          </IonCol>
          <IonCol size-md="3"  >
            <IonLabel>Over-90 Days</IonLabel>
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol size-md="3">
            $0.00
        </IonCol>
          <IonCol size-md="3">
            $0.00
        </IonCol>
          <IonCol size-md="3" >
            $0.00
        </IonCol>
          <IonCol size-md="3" >
            $126.50
       </IonCol>
        </IonRow>
      </IonGrid>
      </IonCardContent>
      </IonCard>
      <IonCard>
        <IonCardContent>
      <IonGrid>
        <IonRow>
          <IonCol size-md="3">
            <IonLabel>From Date</IonLabel>
          </IonCol>

          <IonCol size-md="3">
            <IonDatetime displayFormat="MM/DD/YYYY" placeholder="Select Date" value={selectedDate} onIonChange={e => setSelectedDate(e.detail.value!)}></IonDatetime>
          </IonCol>
        </IonRow>


        <IonRow>
          <IonCol size-md="3">
            <IonLabel>To Date</IonLabel>
          </IonCol>
          <IonCol size-md="3">
            <IonDatetime displayFormat="MM/DD/YYYY" placeholder="Select Date" value={selectedDate} onIonChange={e => setSelectedDate(e.detail.value!)}></IonDatetime>
          </IonCol>
        </IonRow>


        <IonRow>
          <IonCol size-md="3"  >
            <IonLabel>Filter By</IonLabel>
          </IonCol>
          <IonCol size-md="3" >
            <IonSelect value={filterBy} okText="Okay" cancelText="Dismiss" onIonChange={e => setFilterBy(e.detail.value)}>
              <IonSelectOption value="processing">Processing</IonSelectOption>
              <IonSelectOption value="outstanding">Outstanding</IonSelectOption>
              <IonSelectOption value="paid">Paid</IonSelectOption>
            </IonSelect>
          </IonCol>
        </IonRow>
      </IonGrid>
      </IonCardContent>
      </IonCard>
        
    </>

  )
}
export default Filters;