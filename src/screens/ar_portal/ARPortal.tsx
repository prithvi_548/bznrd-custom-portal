import React, { useState } from 'react';
import { IonContent, IonHeader, IonToolbar, IonTitle, IonList, IonItem, IonIcon, IonLabel, useIonViewWillEnter, IonInfiniteScroll, IonInfiniteScrollContent } from '@ionic/react';

import UserInfo from "./components/UserInfo"
import Filters from './components/Filters';
import { IInvoiceApiListInvoiceServerResponse } from '../../types/Invoices';
import { ServiceInvoiceApi } from '../../services/ServiceInvoiceApi';
import MenuHeader from '../../components/MenuHeader';
import IonReactNav from './components/IonReactNav';
import InvoiceDetail from './components/InvoiceDetail';
import techs from './components/techs';

const ARPortal: React.FC = () => {
  const [invoice, setInvoice] = useState<IInvoiceApiListInvoiceServerResponse>();
  const [items, setItems] = useState<IInvoiceApiListInvoiceServerResponse[]>([]);
  const [disableInfiniteScroll, setDisableInfiniteScroll] = useState<boolean>(false);


  // Function to get Invoices from API
  async function fetchInvoices(reset?: boolean) {
    const invoices: IInvoiceApiListInvoiceServerResponse[] = await ServiceInvoiceApi.getInvoices();

    if (invoices && invoices.length > 0) {
      setItems([...items, ...invoices]);
      setDisableInfiniteScroll(invoices.length < 10);
    } else {
      setDisableInfiniteScroll(true);
    }

  }
  useIonViewWillEnter(async () => {
    await fetchInvoices();
  });

  async function searchNext($event: CustomEvent<void>) {
    await fetchInvoices();

    ($event.target as HTMLIonInfiniteScrollElement).complete();
  }


  return (

    <>
      <MenuHeader />
      <IonReactNav detail={() => <InvoiceDetail {...invoice} />}>

        <IonContent fullscreen>

          <UserInfo />
          <Filters />
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonLabel>ID</IonLabel>
              <IonLabel>Inv No</IonLabel>
              <IonLabel>AR Type</IonLabel>
            </IonToolbar>
          </IonHeader>
          <IonList>
            {items.map((invoice: IInvoiceApiListInvoiceServerResponse, i: number) => {
              return (
                <IonItem button className="ion-react-nav-detail-btn" key={i} onClick={() => setInvoice(invoice)}>
                  <IonLabel>{invoice.id}</IonLabel>
                  <IonLabel>{invoice.invNo}</IonLabel>
                  <IonLabel>{invoice.arType}</IonLabel>
                </IonItem>
              )
            })}
          </IonList>

          <IonInfiniteScroll threshold="100px" disabled={disableInfiniteScroll}
            onIonInfinite={(e: CustomEvent<void>) => searchNext(e)}>
            <IonInfiniteScrollContent
              loadingText="Loading more invoices...">
            </IonInfiniteScrollContent>
          </IonInfiniteScroll>

        </IonContent>
      </IonReactNav>
    </>
  );
};
export default ARPortal;