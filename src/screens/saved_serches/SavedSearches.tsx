import { 
  IonContent, IonGrid, IonRow, IonCard,
 
} from '@ionic/react';
import React from 'react';
import { add } from 'ionicons/icons';
import { RouteComponentProps } from 'react-router';
import MenuHeader from '../../components/MenuHeader';
 

const SavedSearches: React.FC<RouteComponentProps> = (props) => {
  return (

    <>
      <MenuHeader />
      <IonContent  >
       <IonCard>
         Saved Searches
       </IonCard>
      </IonContent>
    </>
  );
};

export default SavedSearches;